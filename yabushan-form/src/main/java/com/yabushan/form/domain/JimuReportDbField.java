package com.yabushan.form.domain;

import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 报表对象 jimu_report_db_field
 *
 * @author yabushan
 * @date 2021-07-03
 */
public class JimuReportDbField extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 数据源ID */
    @Excel(name = "数据源ID")
    private String jimuReportDbId;

    /** 字段名 */
    @Excel(name = "字段名")
    private String fieldName;

    /** 字段文本 */
    @Excel(name = "字段文本")
    private String fieldText;

    /** 控件类型 */
    @Excel(name = "控件类型")
    private String widgetType;

    /** 控件宽度 */
    @Excel(name = "控件宽度")
    private Integer widgetWidth;

    /** 排序 */
    @Excel(name = "排序")
    private Integer orderNum;

    /** 查询标识0否1是 默认0 */
    @Excel(name = "查询标识0否1是 默认0")
    private Integer searchFlag;

    /** 查询模式1简单2范围 */
    @Excel(name = "查询模式1简单2范围")
    private Integer searchMode;

    /** 字典编码支持从表中取数据 */
    @Excel(name = "字典编码支持从表中取数据")
    private String dictCode;

    /** 查询默认值 */
    @Excel(name = "查询默认值")
    private String searchValue;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setJimuReportDbId(String jimuReportDbId)
    {
        this.jimuReportDbId = jimuReportDbId;
    }

    public String getJimuReportDbId()
    {
        return jimuReportDbId;
    }
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    public String getFieldName()
    {
        return fieldName;
    }
    public void setFieldText(String fieldText)
    {
        this.fieldText = fieldText;
    }

    public String getFieldText()
    {
        return fieldText;
    }
    public void setWidgetType(String widgetType)
    {
        this.widgetType = widgetType;
    }

    public String getWidgetType()
    {
        return widgetType;
    }
    public void setWidgetWidth(Integer widgetWidth)
    {
        this.widgetWidth = widgetWidth;
    }

    public Integer getWidgetWidth()
    {
        return widgetWidth;
    }
    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }
    public void setSearchFlag(Integer searchFlag)
    {
        this.searchFlag = searchFlag;
    }

    public Integer getSearchFlag()
    {
        return searchFlag;
    }
    public void setSearchMode(Integer searchMode)
    {
        this.searchMode = searchMode;
    }

    public Integer getSearchMode()
    {
        return searchMode;
    }
    public void setDictCode(String dictCode)
    {
        this.dictCode = dictCode;
    }

    public String getDictCode()
    {
        return dictCode;
    }
    public void setSearchValue(String searchValue)
    {
        this.searchValue = searchValue;
    }

    public String getSearchValue()
    {
        return searchValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("jimuReportDbId", getJimuReportDbId())
            .append("fieldName", getFieldName())
            .append("fieldText", getFieldText())
            .append("widgetType", getWidgetType())
            .append("widgetWidth", getWidgetWidth())
            .append("orderNum", getOrderNum())
            .append("searchFlag", getSearchFlag())
            .append("searchMode", getSearchMode())
            .append("dictCode", getDictCode())
            .append("searchValue", getSearchValue())
            .toString();
    }
}

package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.UumUserinfo;
import com.yabushan.system.service.IUumUserinfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 租户用户Controller
 *
 * @author yabushan
 * @date 2021-04-18
 */
@RestController
@RequestMapping("/system/userinfo")
public class UumUserinfoController extends BaseController
{
    @Autowired
    private IUumUserinfoService uumUserinfoService;

    /**
     * 查询租户用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:userinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(UumUserinfo uumUserinfo)
    {
        startPage();
        List<UumUserinfo> list = uumUserinfoService.selectUumUserinfoList(uumUserinfo);
        return getDataTable(list);
    }

    /**
     * 导出租户用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:userinfo:export')")
    @Log(title = "租户用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UumUserinfo uumUserinfo)
    {
        List<UumUserinfo> list = uumUserinfoService.selectUumUserinfoList(uumUserinfo);
        ExcelUtil<UumUserinfo> util = new ExcelUtil<UumUserinfo>(UumUserinfo.class);
        return util.exportExcel(list, "userinfo");
    }

    /**
     * 获取租户用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:userinfo:query')")
    @GetMapping(value = "/{employeeId}")
    public AjaxResult getInfo(@PathVariable("employeeId") String employeeId)
    {
        return AjaxResult.success(uumUserinfoService.selectUumUserinfoById(employeeId));
    }

    /**
     * 新增租户用户
     */
    @PreAuthorize("@ss.hasPermi('system:userinfo:add')")
    @Log(title = "租户用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UumUserinfo uumUserinfo)
    {
        return toAjax(uumUserinfoService.insertUumUserinfo(uumUserinfo));
    }

    /**
     * 修改租户用户
     */
    @PreAuthorize("@ss.hasPermi('system:userinfo:edit')")
    @Log(title = "租户用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UumUserinfo uumUserinfo)
    {
        return toAjax(uumUserinfoService.updateUumUserinfo(uumUserinfo));
    }

    /**
     * 删除租户用户
     */
    @PreAuthorize("@ss.hasPermi('system:userinfo:remove')")
    @Log(title = "租户用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{employeeIds}")
    public AjaxResult remove(@PathVariable String[] employeeIds)
    {
        return toAjax(uumUserinfoService.deleteUumUserinfoByIds(employeeIds));
    }
}

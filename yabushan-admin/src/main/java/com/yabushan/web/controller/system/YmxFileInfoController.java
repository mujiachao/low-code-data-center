package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxFileInfo;
import com.yabushan.system.service.IYmxFileInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 附件Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxfileinfo")
public class YmxFileInfoController extends BaseController
{
    @Autowired
    private IYmxFileInfoService ymxFileInfoService;

    /**
     * 查询附件列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxfileinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxFileInfo ymxFileInfo)
    {
        startPage();
        List<YmxFileInfo> list = ymxFileInfoService.selectYmxFileInfoList(ymxFileInfo);
        return getDataTable(list);
    }

    /**
     * 导出附件列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxfileinfo:export')")
    @Log(title = "附件", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxFileInfo ymxFileInfo)
    {
        List<YmxFileInfo> list = ymxFileInfoService.selectYmxFileInfoList(ymxFileInfo);
        ExcelUtil<YmxFileInfo> util = new ExcelUtil<YmxFileInfo>(YmxFileInfo.class);
        return util.exportExcel(list, "ymxfileinfo");
    }

    /**
     * 获取附件详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxfileinfo:query')")
    @GetMapping(value = "/{fileId}")
    public AjaxResult getInfo(@PathVariable("fileId") String fileId)
    {
        return AjaxResult.success(ymxFileInfoService.selectYmxFileInfoById(fileId));
    }

    /**
     * 新增附件
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxfileinfo:add')")
    @Log(title = "附件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxFileInfo ymxFileInfo)
    {
        return toAjax(ymxFileInfoService.insertYmxFileInfo(ymxFileInfo));
    }

    /**
     * 修改附件
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxfileinfo:edit')")
    @Log(title = "附件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxFileInfo ymxFileInfo)
    {
        return toAjax(ymxFileInfoService.updateYmxFileInfo(ymxFileInfo));
    }

    /**
     * 删除附件
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxfileinfo:remove')")
    @Log(title = "附件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{fileIds}")
    public AjaxResult remove(@PathVariable String[] fileIds)
    {
        return toAjax(ymxFileInfoService.deleteYmxFileInfoByIds(fileIds));
    }
}

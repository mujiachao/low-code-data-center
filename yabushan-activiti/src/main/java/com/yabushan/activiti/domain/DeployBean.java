package com.yabushan.activiti.domain;

/**
 * 部署
 * @author yabushan
 *
 */
public class DeployBean {

	private String deployId;
	private String deployName;
	public String getDeployId() {
		return deployId;
	}
	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}
	public String getDeployName() {
		return deployName;
	}
	public void setDeployName(String deployName) {
		this.deployName = deployName;
	}

}

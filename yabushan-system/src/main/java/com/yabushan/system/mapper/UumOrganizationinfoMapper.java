package com.yabushan.system.mapper;

import java.util.List;
import com.yabushan.system.domain.UumOrganizationinfo;
import com.yabushan.system.utils.UumOrgInfo;

/**
 * 租户组织Mapper接口
 *
 * @author yabushan
 * @date 2021-04-17
 */
public interface UumOrganizationinfoMapper
{
    /**
     * 查询租户组织
     *
     * @param ouguid 租户组织ID
     * @return 租户组织
     */
    public UumOrganizationinfo selectUumOrganizationinfoById(String ouguid);

    /**
     * 查询租户组织列表
     *
     * @param uumOrganizationinfo 租户组织
     * @return 租户组织集合
     */
    public List<UumOrganizationinfo> selectUumOrganizationinfoList(UumOrganizationinfo uumOrganizationinfo);

    /**
     * 新增租户组织
     *
     * @param uumOrganizationinfo 租户组织
     * @return 结果
     */
    public int insertUumOrganizationinfo(UumOrganizationinfo uumOrganizationinfo);

    /**
     * 修改租户组织
     *
     * @param uumOrganizationinfo 租户组织
     * @return 结果
     */
    public int updateUumOrganizationinfo(UumOrganizationinfo uumOrganizationinfo);

    /**
     * 删除租户组织
     *
     * @param ouguid 租户组织ID
     * @return 结果
     */
    public int deleteUumOrganizationinfoById(String ouguid);

    /**
     * 批量删除租户组织
     *
     * @param ouguids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUumOrganizationinfoByIds(String[] ouguids);

    public List<UumOrgInfo> selectUumOrgInfoList(UumOrgInfo uumOrgInfo);
}
